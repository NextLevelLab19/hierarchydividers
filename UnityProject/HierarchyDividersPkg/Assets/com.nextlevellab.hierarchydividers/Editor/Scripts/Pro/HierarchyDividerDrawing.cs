﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ManasparkAssets
{
	namespace HierarchyDividersPro
	{
		[InitializeOnLoad]
		[Serializable]
		public class HierarchyDividerDrawing
		{
			private static Material spriteMatTransparent;
			private static Material spriteMatOverrideColor;
			
			[SerializeField]
			public static List<HierarchyDividerProValues> Dividers;
			
			private static Vector2 offset = new Vector2(0, 2);

			static HierarchyDividerDrawing()
			{
				LoadMaterials();
				EditorApplication.hierarchyWindowItemOnGUI += HandleHierarchyWindowItemOnGUI;
			}

			private static string getThisDirectory([CallerFilePath] string _sourceFilePath = "")
			{
				return _sourceFilePath;
			}
			
			public static void LoadMaterials()
			{
				string _editorDirectory = getThisDirectory();
				for (int i = 0; i < 3; i++)
				{
					_editorDirectory = System.IO.Directory.GetParent(_editorDirectory).ToString();
				}
				_editorDirectory = _editorDirectory.Replace(System.IO.Directory.GetCurrentDirectory(), "").Substring(1);

//				spriteMatTransparent = (Material)AssetDatabase.LoadAssetAtPath("Assets/Manaspark Studio/Hierarchy Dividers/Editor/Basic/Material/EditorSpriteTransparent.mat", typeof(Material));
//				spriteMatOverrideColor = (Material)AssetDatabase.LoadAssetAtPath("Assets/Manaspark Studio/Hierarchy Dividers/Editor/Pro/Material/EditorSpriteOverrideColor.mat", typeof(Material));
				spriteMatTransparent = (Material)AssetDatabase.LoadAssetAtPath($"{_editorDirectory}/Editor/Basic/Material/EditorSpriteTransparent.mat", typeof(Material));
				if(spriteMatTransparent == null)
					Debug.LogWarning($"could not load: {_editorDirectory}/Editor/Basic/Material/EditorSpriteTransparent.mat");
				spriteMatOverrideColor = (Material)AssetDatabase.LoadAssetAtPath($"{_editorDirectory}/Editor/Pro/Material/EditorSpriteOverrideColor.mat", typeof(Material));
				if(spriteMatOverrideColor == null)
					Debug.LogWarning($"could not load: {_editorDirectory}/Pro/Material/EditorSpriteOverrideColor.mat");
			}

			private static void HandleHierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
			{
				if(spriteMatTransparent == null || spriteMatOverrideColor == null)
					LoadMaterials();
				
				GameObject obj = (GameObject) EditorUtility.InstanceIDToObject(instanceID);

				if(obj == null)
					return;

				HierarchyDividerPro found = obj.GetComponent<HierarchyDividerPro>();
				if(found == null)
					return;
				
				HierarchyDividerProValues _foundDividerPro = found.HDV;
				
				if (Dividers == null || Dividers.Count == 0)
				{
					Dividers = new List<HierarchyDividerProValues>();
					Dividers.AddRange(GameObject.FindObjectsOfType<HierarchyDividerPro>().ToList().Select(x => x.HDV).ToList());
				}

				try
				{
					HierarchyDividerProValues _dividerPro = Dividers.Find(x => x.DividerID == _foundDividerPro.DividerID);
					if (_dividerPro == null)
					{
						Dividers.AddRange(GameObject.FindObjectsOfType<HierarchyDividerPro>().ToList().Select(x => x.HDV).ToList());
					}
					
					if (obj != null && _dividerPro != null)
					{
						
						Color fontColor = _dividerPro.fontColor;
						Color bgColor = new Color(_dividerPro.bgColor.r, _dividerPro.bgColor.g, _dividerPro.bgColor.b, 1f);
					
						Color standardBg = EditorGUIUtility.isProSkin ?
							new Color(0.22f, 0.22f, 0.22f)
							: new Color(.76f, .76f, .76f);
						
						Color _selectionColor = EditorGUIUtility.isProSkin ? new Color(0.24f, 0.37f, 0.59f) : new Color(0.24f, 0.48f, 0.90f);

						bool selected = Selection.instanceIDs.Contains(instanceID);
						if (selected)
						{
							fontColor = Color.white;
							bgColor = _selectionColor;
						}

						float spriteSize = 15f;
						
						#if UNITY_2019_2
						selectionRect.x += 18;
						selectionRect.width -= 4;
						#elif UNITY_2019_1_OR_NEWER
						selectionRect.x += 20;
						selectionRect.width -= 6;
						#endif
					
						Rect offsetRect = new Rect(selectionRect.position + offset, selectionRect.size);
						
						#if UNITY_2018_3
						offsetRect.width += 15f;
						#endif
						
						if (_dividerPro.hasBgColor || selected)
						{
							if (_dividerPro.hasRightSprite)
							{
								#if UNITY_2019_1_OR_NEWER
								EditorGUI.DrawRect(new Rect(selectionRect.x, selectionRect.y, offsetRect.width - (spriteSize + 4) - 5, offsetRect.height), bgColor);
								#else
								EditorGUI.DrawRect(new Rect(selectionRect.x, selectionRect.y, offsetRect.width - (spriteSize + 4), offsetRect.height), bgColor);
								#endif
							}
							else
							{
								#if UNITY_2019_2
								EditorGUI.DrawRect(new Rect(selectionRect.x, selectionRect.y, offsetRect.width + 2, offsetRect.height), bgColor);
#else
								EditorGUI.DrawRect(new Rect(selectionRect.x, selectionRect.y, offsetRect.width, offsetRect.height), bgColor);
								#endif
}
						}
						else
						{
							#if UNITY_2019_2
							EditorGUI.DrawRect(new Rect(selectionRect.x - 45, selectionRect.y, offsetRect.width + 45, selectionRect.height), standardBg);
							#else
							EditorGUI.DrawRect(new Rect(selectionRect.x, selectionRect.y, offsetRect.width, selectionRect.height), standardBg);
							#endif
						}
					
						GUIStyle _style = new GUIStyle();
						if (!_dividerPro.boldFont && !_dividerPro.italicFont)
							_style.fontStyle = FontStyle.Normal;
						if (_dividerPro.boldFont && !_dividerPro.italicFont)
							_style.fontStyle = FontStyle.Bold;
						else if(!_dividerPro.boldFont && _dividerPro.italicFont)
							_style.fontStyle = FontStyle.Italic;
						else if(_dividerPro.boldFont && _dividerPro.italicFont)
							_style.fontStyle = FontStyle.BoldAndItalic;

						_style.normal = _dividerPro.hasFontColor || selected
							? new GUIStyleState() {textColor = fontColor}
							: new GUIStyleState();
						
						_style.alignment = TextAnchor.UpperCenter;
						
//						if (_dividerPro.hasRightSprite)
//							EditorGUI.LabelField(new Rect(offsetRect.x, offsetRect.y, offsetRect.width - (spriteSize + 4f), offsetRect.height), obj.name, _style);
//						else
//							EditorGUI.LabelField(offsetRect, obj.name, _style);
						EditorGUI.LabelField(new Rect(offsetRect.x, offsetRect.y, offsetRect.width - (spriteSize + 4f), offsetRect.height), obj.name, _style);
						
						#if UNITY_2019_2
						EditorGUI.DrawRect(new Rect(16f, offsetRect.y - 2, 45, EditorGUIUtility.singleLineHeight), selected? _selectionColor : standardBg);
						#elif UNITY_2019_1_OR_NEWER
						EditorGUI.DrawRect(new Rect(25f, offsetRect.y - 2, 30, EditorGUIUtility.singleLineHeight), selected? _selectionColor : standardBg);
						#else
						EditorGUI.DrawRect(new Rect(0f, offsetRect.y - 2, 30, EditorGUIUtility.singleLineHeight), selected? _selectionColor : standardBg);
						#endif
						
						if (_dividerPro.hasLeftSprite)
						{
							Rect _leftSpriteRect = new Rect(offsetRect.x - (spriteSize * 1.5f), offsetRect.y - 2,
							                                spriteSize, EditorGUIUtility.singleLineHeight);
							#if UNITY_2019_2
							_leftSpriteRect.x += 1;
							#elif UNITY_2019_1_OR_NEWER
							_leftSpriteRect.x += 3;
							#endif
							
							if (_dividerPro.HasOverrideColor_leftSprite)
							{
								DrawSprite(_leftSpriteRect,
								           _dividerPro.leftSprite, spriteSize, _dividerPro.LeftScale, _dividerPro.OverrideColor_leftSprite);
							}
							else
								DrawSprite(_leftSpriteRect,
								           _dividerPro.leftSprite, spriteSize, _dividerPro.LeftScale);
						}

						if (_dividerPro.hasRightSprite)
						{
							Rect _rightSpriteRect = new Rect(offsetRect.x + offsetRect.width - spriteSize - 3f,
							                                 offsetRect.y - 2, spriteSize,
							                                 EditorGUIUtility.singleLineHeight);
							
							#if UNITY_2019_1_OR_NEWER
							EditorGUI.DrawRect(new Rect(_rightSpriteRect.x - 5f, _rightSpriteRect.y, _rightSpriteRect.width + 55f, _rightSpriteRect.height), selected? _selectionColor : standardBg);
							_rightSpriteRect.x -= 2;
							#else
							EditorGUI.DrawRect(new Rect(_rightSpriteRect.x - 1f, _rightSpriteRect.y, _rightSpriteRect.width + 50f, _rightSpriteRect.height), selected? _selectionColor : standardBg);
							#endif

							if (_dividerPro.HasOverrideColor_rightSprite)
							{
								DrawSprite(_rightSpriteRect,
								           _dividerPro.rightSprite, spriteSize, _dividerPro.RightScale, _dividerPro.OverrideColor_rightSprite);
							}
							else
								DrawSprite(_rightSpriteRect,
								           _dividerPro.rightSprite, spriteSize, _dividerPro.RightScale);
						}
					}
				}
				catch (Exception e)
				{
					throw e;
				}				
			}

			public static void DrawSprite(Rect rect, Texture _tex, float _size, float _scale)
			{
				calculateIconRect(ref rect, _size, _scale);

				EditorGUI.DrawPreviewTexture(rect, _tex, spriteMatTransparent, ScaleMode.ScaleToFit, 0f);
			}
			
			public static void DrawSprite(Rect rect, Texture _tex, float _size, float _scale, Color _overrideColor)
			{
				calculateIconRect(ref rect, _size, _scale);
				
				Material _tempMat = new Material(spriteMatOverrideColor);
				_tempMat.SetColor("_Color", _overrideColor);
				
				EditorGUI.DrawPreviewTexture(rect, _tex, _tempMat, ScaleMode.ScaleToFit, 0f);
			}

			private static void calculateIconRect(ref Rect _rect, float _size, float _scale)
			{
				float _scaledSize = _scale * _size;
				_rect.x += 0.5f * EditorGUIUtility.singleLineHeight - 0.5f * _scaledSize;
				_rect.y += 0.5f * EditorGUIUtility.singleLineHeight - 0.5f * _scaledSize;
				
				_rect.width = _rect.height = _scaledSize;
			}
		}
	}
}
#endif